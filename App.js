/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import 'react-native-gesture-handler';
import * as React from 'react';
import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  Button
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Dialog, { DialogContent, DialogFooter, DialogButton } from 'react-native-popup-dialog';

const storage = new Storage({
  size: 1000,
  storageBackend: AsyncStorage,
  defaultExpires: null,
  enableCache: true,
  sync:{

  }
})

const DATA = [
  {
    id: '1',
    nama: 'kacamata',
    bought: 'false',
    image: <Image source={require('./assets/kacamata.png')}/>,
    harga: 150
  },
  {
    id: '2',
    nama: 'topi',
    bought: 'false',
    image: <Image source={require('./assets/topi.png')}/>,
    harga: 100
  },
  {
    id: '3',
    nama: 'kemeja',
    bought: 'false',
    image: <Image source={require('./assets/baju.png')}/>,
    harga: 200
  },
  {
    id: '4',
    nama: 'permen',
    bought: 'false',
    image: <Image source={require('./assets/permen.png')}/>,
    harga: 50
  }
];

function ListBarang({item,id,nama,harga,selectItem,bought}){
  return(
      <TouchableOpacity
        disabled={bought==true?true:null}
        onPress={()=>selectItem(id,nama,harga)}
        style={
          { backgroundColor: bought==true ? '#7f8c8d' : 'white' }
        }
      >
        {bought == true? <Text>DIBELI</Text>:null}
        {item}
      </TouchableOpacity>
  );
}

let namaBarang;
let hargaBarang;
let idBarang;
let availableBarang = [];

function HomeScreen({navigation}) {
  React.useEffect(()=>{
    console.disableYellowBox = true;
  },[]);
  const[isVisible,showDialog] = React.useState(false);
  const[isBeli,showBeli] = React.useState(false);
  const[bought,setBought] = React.useState(new Map());
  const confirmBox = (id,barang,harga) => {
    showDialog(isVisible => !isVisible);
    idBarang = id;
    namaBarang = barang;
    hargaBarang = harga;
  }
  const buyAnimation = () => {
    availableBarang[idBarang] = false
    const newBought = new Map(bought);
    newBought.set(idBarang,!bought.get(idBarang))
    setBought(newBought)
    showBeli(isBeli => !isBeli);
  }
  const cekBarang = () => {
    for (let index = 0; index < availableBarang.length; index++) {
      console.log("barang ke-"+index+": "+availableBarang[index]);
    }
  }
  return (
    <View>
      <Button
        title="Go to profile"
        onPress={()=>navigation.navigate('Profile')}
      />
      <FlatList
        data={DATA}
        renderItem={({ item }) => (
          <ListBarang 
            item={item.image} 
            selectItem={confirmBox}
            bought={!!bought.get(item.id)}
            id={item.id} 
            nama={item.nama} 
            harga={item.harga}
          />
        )}
        extraData={bought}
        keyExtractor={item => item.id}
      />
      <Dialog
        visible = {isVisible}
        footer={
          <DialogFooter>
            <DialogButton
              text="Lain Kali"
              onPress={()=>{
                confirmBox()
              }}
            />
            <DialogButton
              text="Beli"
              onPress={()=>{
                showDialog(isVisible => !isVisible);
                buyAnimation()
                cekBarang()
              }}
            />
          </DialogFooter>
        }
      >
        <DialogContent>
          <Text>Beli {namaBarang} ini dengan {hargaBarang} diamond?</Text>
        </DialogContent>
      </Dialog>
      <Dialog
        visible = {isBeli}
        footer={
          <DialogFooter>
            <DialogButton
              text="OK"
              onPress={()=>{
                showBeli(isBeli => !isBeli);
              }}
            />
          </DialogFooter>
        }
      >
        <DialogContent>
          <Text>Terima kasih telah membeli {namaBarang} ini</Text>
          <Text>Silahkan memberikan aksesoris ini</Text>
          <Text>kepada bebras kamu ya!</Text>
        </DialogContent>
      </Dialog>
    </View>
  );
}

function ProfileScreen({navigation}){
  return(
    <Text>Profile</Text>
  )
}

const Stack = createStackNavigator();

function App(){
  for (let index = 0; index <= 4; index++) {
    availableBarang.push(true)
  }
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Profile" component={ProfileScreen}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;
